package com.postnord.api.kafka.control;

import com.postnord.api.kafka.control.dto.Movie;


import org.eclipse.microprofile.reactive.messaging.Channel;
import org.eclipse.microprofile.reactive.messaging.Emitter;

import io.smallrye.mutiny.Uni;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.json.bind.Jsonb;
import jakarta.json.bind.JsonbBuilder;
import jakarta.ws.rs.core.Response;

@ApplicationScoped
public class MovieProducer {

    private static final Jsonb jsonb = JsonbBuilder.create();

    @Inject
    @Channel("movies-out")
    Emitter<String> emitter;

    public Uni<Response> sendMovie(Movie movie) {

        System.out.println(jsonb.toJson(movie, Movie.class));

        emitter.send(jsonb.toJson(movie, Movie.class));
        return Uni.createFrom().item(Response.ok("The message is being put to the topic").build());
    }
}
