package com.postnord.api.kafka.external.boundary;

import com.postnord.api.kafka.control.MovieProducer;
import com.postnord.api.kafka.control.dto.Movie;

import io.smallrye.mutiny.Uni;
import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;


@Path("/movie")
public class MovieResource {


    @Inject
    MovieProducer movieProducer;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Uni<Response> send(final Movie movie) {
        return movieProducer.sendMovie(movie);
    }
}
