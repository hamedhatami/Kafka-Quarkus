package com.postnord.api;

import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;

import static io.restassured.RestAssured.given;

@QuarkusTest
class MovieResourceTest {

    @Test
    void testHelloEndpoint() {
        given()
                .contentType(ContentType.JSON)
                .body("{\"year\":1972, \"title\":\"The Godfather\"}")
                .when()
                .post("/movie")
                .then()
                .statusCode(200);
    }

}
