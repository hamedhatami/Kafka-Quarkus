# Kafka-Quarkus project

This project uses Quarkus, the Supersonic Subatomic Java Framework.

If you want to learn more about Quarkus, please visit its website: https://quarkus.io/ .

## Running the application in dev mode

You can run your application in dev mode that enables live coding using:
```shell script
mvn clean install
```

## running the application

The application can be packaged using:
```shell script
java -jar target/quarkus-app/quarkus-run.jar
```
It produces the `quarkus-run.jar` file in the `target/quarkus-app/` directory.

## Provided examples

```shell script
curl -i -X POST "http://localhost:8080/movie" -H "Content-Type: application/json" -d '{"year":2020, "title":"The Hamed Hatami"}' 
```
